# frozen_string_literal: true

module Ai
  module UserAuthorizable
    extend ActiveSupport::Concern

    GROUP_WITH_AI_ENABLED_CACHE_PERIOD = 1.hour
    GROUP_WITH_AI_ENABLED_CACHE_KEY = 'group_with_ai_enabled'
    GROUP_WITH_GA_AI_ENABLED_CACHE_KEY = 'group_with_ga_ai_enabled'

    GROUP_WITH_AI_CHAT_ENABLED_CACHE_PERIOD = 1.hour
    GROUP_WITH_AI_CHAT_ENABLED_CACHE_KEY = 'group_with_ai_chat_enabled'
    GROUP_IDS_WITH_AI_CHAT_ENABLED_CACHE_KEY = 'group_ids_with_ai_chat_enabled'

    GROUP_REQUIRES_LICENSED_SEAT_FOR_CHAT_CACHE_PERIOD = 10.minutes
    GROUP_REQUIRES_LICENSED_SEAT_FOR_CHAT_CACHE_KEY = 'group_requires_licensed_seat_for_chat'

    DUO_PRO_ADD_ON_CACHE_KEY = 'user-%{user_id}-code-suggestions-add-on-cache'

    included do
      def duo_pro_add_on_available_namespace_ids
        cache_key = format(DUO_PRO_ADD_ON_CACHE_KEY, user_id: id)

        Rails.cache.fetch(cache_key, expires_in: 1.hour) do
          GitlabSubscriptions::UserAddOnAssignment.by_user(self).for_active_gitlab_duo_pro_purchase
            .pluck('subscription_add_on_purchases.namespace_id') # rubocop: disable Database/AvoidUsingPluckWithoutLimit -- limited to a single user's purchases
        end
      end

      def duo_pro_cache_key_formatted
        format(User::DUO_PRO_ADD_ON_CACHE_KEY, user_id: id)
      end

      def eligible_for_self_managed_gitlab_duo_pro?
        return false if gitlab_com_subscription?

        active? && !bot? && !ghost?
      end

      # rubocop: disable Database/AvoidUsingPluckWithoutLimit -- limited to a single user's groups
      def billable_gitlab_duo_pro_root_group_ids
        return unless gitlab_com_subscription?

        group_ids_from_project_authorizaton = Project.where(id: project_authorizations.non_guests.select(:project_id))
          .pluck(:namespace_id)
        group_ids_from_memberships = GroupMember.with_user(self).active.non_guests.pluck(:source_id)
        group_ids_from_linked_groups = GroupGroupLink.non_guests.where(shared_with_group_id: group_ids_from_memberships)
          .pluck(:shared_group_id)

        Group.where(
          id: group_ids_from_project_authorizaton | group_ids_from_memberships | group_ids_from_linked_groups
        ).pluck(Arel.sql('traversal_ids[1]')).uniq
      end
      # rubocop: enable Database/AvoidUsingPluckWithoutLimit

      def any_group_with_ai_available?
        Rails.cache.fetch(['users', id, GROUP_WITH_AI_ENABLED_CACHE_KEY],
          expires_in: GROUP_WITH_AI_ENABLED_CACHE_PERIOD) do
          member_namespaces.namespace_settings_with_ai_features_enabled.with_ai_supported_plan.any?
        end
      end

      def any_group_with_ga_ai_available?(service_name)
        Rails.cache.fetch(['users', id, GROUP_WITH_GA_AI_ENABLED_CACHE_KEY],
          expires_in: GROUP_WITH_AI_ENABLED_CACHE_PERIOD) do
          groups = member_namespaces.with_ai_supported_plan
          groups_that_require_licensed_seat = groups.select do |group|
            ::Feature.enabled?(:duo_chat_requires_licensed_seat, group)
          end

          if groups.any? && groups_that_require_licensed_seat.any?
            ::CloudConnector::AvailableServices.find_by_name(service_name).allowed_for?(self)
          else
            groups.any?
          end
        end
      end

      def ai_chat_enabled_namespace_ids
        Rails.cache.fetch(['users', id, GROUP_IDS_WITH_AI_CHAT_ENABLED_CACHE_KEY],
          expires_in: GROUP_WITH_AI_CHAT_ENABLED_CACHE_PERIOD) do
          groups = member_namespaces.with_ai_supported_plan(:ai_chat)
          groups.pluck(Arel.sql('DISTINCT traversal_ids[1]')) # rubocop: disable Database/AvoidUsingPluckWithoutLimit -- limited to a single user's groups
        end
      end

      def any_group_with_ai_chat_available?
        Rails.cache.fetch(['users', id, GROUP_WITH_AI_CHAT_ENABLED_CACHE_KEY],
          expires_in: GROUP_WITH_AI_CHAT_ENABLED_CACHE_PERIOD) do
          groups = member_namespaces.with_ai_supported_plan(:ai_chat)
          groups_that_require_licensed_seat_for_chat = groups.select do |group|
            ::Feature.enabled?(:duo_chat_requires_licensed_seat, group)
          end

          if groups.any? && groups_that_require_licensed_seat_for_chat.any?
            ::CloudConnector::AvailableServices.find_by_name(:duo_chat).allowed_for?(self)
          else
            groups.any?
          end
        end
      end

      def belongs_to_group_requires_licensed_seat_for_chat?
        Rails.cache.fetch(['users', id, GROUP_REQUIRES_LICENSED_SEAT_FOR_CHAT_CACHE_KEY],
          expires_in: GROUP_REQUIRES_LICENSED_SEAT_FOR_CHAT_CACHE_PERIOD) do
          group_ids = ::Feature.group_ids_for(:duo_chat_requires_licensed_seat)
          member_namespaces.by_root_id(group_ids).any?
        end
      end

      def allowed_to_use?(ai_feature, service_name: nil, licensed_feature: :ai_features, &block)
        feature_data = Gitlab::Llm::Utils::AiFeaturesCatalogue.search_by_name(ai_feature)
        return false unless feature_data

        service = CloudConnector::AvailableServices.find_by_name(service_name || ai_feature)
        return false if service.name == :missing_service

        # If the user has any relevant add-on purchase, they always have access to this service
        purchases = service.add_on_purchases.assigned_to_user(self)
        if purchases.any?
          yield purchases.uniq_namespace_ids if block

          return true
        end

        # If the user doesn't have add-on purchases and the service isn't free, they don't have access
        return false if !service.free_access? ||
          (service.name == :self_hosted_models && Feature.enabled?(:self_hosted_models_beta_ended, self))

        if Gitlab::Saas.feature_available?(:duo_chat_on_saas)
          licensed_to_use_in_com?(feature_data[:maturity], &block)
        else
          licensed_to_use_in_sm?(licensed_feature)
        end
      end

      def allowed_by_namespace_ids(*args, **kwargs)
        allowed_to_use?(*args, **kwargs) { |namespace_ids| return namespace_ids } # rubocop:disable Cop/AvoidReturnFromBlocks -- Early return if namespace ids are yielded

        []
      end

      private

      def licensed_to_use_in_com?(maturity)
        namespaces = member_namespaces.with_ai_supported_plan

        requiring_seat = namespaces.select do |namespace|
          ::Feature.enabled?(:duo_chat_requires_licensed_seat, namespace)
        end

        # If any namespace requires a licensed seat and we've reached this point after checking for any seat assigned
        # to the user, they don't have access
        return false if requiring_seat.any?

        namespaces = namespaces.namespace_settings_with_ai_features_enabled if maturity != :ga

        yield namespaces.ids if block_given?

        namespaces.any?
      end

      def licensed_to_use_in_sm?(licensed_feature)
        License.feature_available?(licensed_feature)
      end
    end

    class_methods do
      def clear_group_with_ai_available_cache(ids)
        cache_keys_ai_features = Array.wrap(ids).map { |id| ["users", id, GROUP_WITH_AI_ENABLED_CACHE_KEY] }
        cache_keys_ga_ai_features = Array.wrap(ids).map { |id| ["users", id, GROUP_WITH_GA_AI_ENABLED_CACHE_KEY] }
        cache_keys_ai_chat = Array.wrap(ids).map { |id| ["users", id, GROUP_WITH_AI_CHAT_ENABLED_CACHE_KEY] }
        cache_keys_ai_chat_group_ids = Array.wrap(ids).map do |id|
          ["users", id, GROUP_IDS_WITH_AI_CHAT_ENABLED_CACHE_KEY]
        end
        cache_keys_requires_licensed_seat = Array.wrap(ids).map do |id|
          ["users", id, GROUP_REQUIRES_LICENSED_SEAT_FOR_CHAT_CACHE_KEY]
        end

        cache_keys = cache_keys_ai_features + cache_keys_ga_ai_features + cache_keys_ai_chat +
          cache_keys_ai_chat_group_ids + cache_keys_requires_licensed_seat
        ::Gitlab::Instrumentation::RedisClusterValidator.allow_cross_slot_commands do
          Rails.cache.delete_multi(cache_keys)
        end
      end
    end
  end
end
